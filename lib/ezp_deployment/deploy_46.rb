# This is a generic deployment recipie for deployment eZ Publish 4.6.
require 'capistrano'

class CapeZDeploy46
  def self.load_into(cap_config)
    cap_config.load do
      after "deploy:update", "deploy:cleanup"

      namespace :deploy do
        desc "This is here to override the original :restart"
        task :restart, :roles => :web do
          # do nothing but overide the default
        end

        desc "Performance last set of deployment tasks"
        task :finalize_update, :roles => :web do
          run "chmod -R g+w #{latest_release}" if fetch(:group_writable, true)

          create_symlinks
          clear_cache
        end

        desc "Create additional eZpublish directories and set permissions after initial setup"
        task :after_setup, :roles => :web do
          # create storage and upload directories
          run "mkdir #{deploy_to}/#{shared_dir}/var"
          run "mkdir #{deploy_to}/#{shared_dir}/tmp"
          # set permissions
          run "chmod 777 #{deploy_to}/#{shared_dir}/var"
          run "chmod 777 #{deploy_to}/#{shared_dir}/tmp"
        end

        desc "Copy storage folder from deploy user's home to shared directory"
        task :copy_var, :roles => :web do
          # copy the var folder
          run "cp -R /home/#{user}/var #{deploy_to}/#{shared_dir}"
          # reset permissions
          run "chmod -R 777 #{deploy_to}/#{shared_dir}/var"
        end

        desc "Create symlinks to shared data such as config files and var"
        task :create_symlinks, :roles => :web do
          run "cd #{current_release} && ./symsync.sh #{environment}"
          run "if [ -e #{current_release}/var ]; then unlink #{current_release}/var; fi"
        end

        desc "Clear the eZPublish caches"
        task :clear_cache, :roles => :web do
          generate_autoloads
          clear_local_caches
          clear_global_caches
          run "cd #{current_release} && mkdir -p var/cache && chmod 777 -R var/cache"
          run "cd #{current_release} && mkdir -p var/log && chmod 777 -R var/log"
          run "cd #{current_release} && mkdir -p var/storage && chmod 777 -R var/storage"

          # Touch all files to make sure that APC clears its cache
          run "cd #{current_release} && find . |xargs touch"
        end

        desc "Generate eZ Publish autoloads"
        task :generate_autoloads, :roles => :web do
          run "cd #{current_release} && php bin/php/ezpgenerateautoloads.php"
          run "cd #{current_release} && php bin/php/ezpgenerateautoloads.php -o"
        end

        desc "Clear local (non clustered) caches on each cluster nodes"
        task :clear_local_caches, :roles => :web do
          run "cd #{current_release} && php bin/php/ezcache.php --clear-tag=ini > /dev/null"
          run "cd #{current_release} && php bin/php/ezcache.php --clear-id=template-override > /dev/null"
          run "cd #{current_release} && php bin/php/ezcache.php --clear-id=design_base > /dev/null"
          run "cd #{current_release} && php bin/php/ezcache.php --clear-id=active_extensions > /dev/null"
          run "cd #{current_release} && php bin/php/ezcache.php --clear-id=template > /dev/null"
        end

        desc "Clear all caches from the master server (taking care of all clustered caches at once)"
        task :clear_global_caches, :roles => :master do
          run "cd #{current_release} && php bin/php/ezcache.php --clear-all > /dev/null"
        end
      end
    end
  end
end

if Capistrano::Configuration.instance
  CapeZDeploy46.load_into(Capistrano::Configuration.instance)
end
