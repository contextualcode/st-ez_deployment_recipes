# This file contains additional Capistrano deployment tasks for managing static
# assets with Compass and eZ Publish. Compiled CSS will be uploaded to each
# server as part of the deployment process.
#
# It's assumed that compass lives in vendor/bin/compass and that compass
# compiles CSS into #{design_dir}/stylesheets/compiled/. Each file in this
# directory will be uploaded to the server to #{design_dir}/stylesheets/.
# E.g design/mydesign/stylesheets/compiled/mysite.css will be
# copied to the server as design/mydesign/stylesheets/mysite_<MD5>.css and
# design/mydesign/stylesheets/mysite.css.
#
# This file also contains a short hand for launching compass in watch mode by
# running `cap assets:watch`.
require 'capistrano'

class CapeZAssets
  def self.load_into(cap_config)
    cap_config.load do
      before "deploy", "assets:compile_css"
      before "deploy:finalize_update", "assets:upload_css"

      namespace :assets do
        desc "Compiles files using Compass"
        task :compile_css do
          run_locally "vendor/bin/compass compile -e production --force #{design_dir}"
        end

        desc "Uploads compiled assets to server"
        task :upload_css do
          for f in Dir["#{design_dir}/stylesheets/compiled/*"]
            md5 = Digest::MD5.file(f).hexdigest
            extension = File.extname(f)
            filename = File.basename(f, extension)
            new_file_name = "#{design_dir}/stylesheets/#{filename}#{extension}"
            md5_file_name = "#{design_dir}/stylesheets/#{filename}_#{md5}#{extension}"

            upload(f, "#{latest_release}/#{new_file_name}")
            run("cd #{latest_release} && cp '#{new_file_name}' '#{md5_file_name}'")
          end
        end

        desc "Watches SASS files and recompiles them on-the-fly when they change"
        task :watch do
          exec "vendor/bin/compass watch #{design_dir} -e development"
        end
      end
    end
  end
end

if Capistrano::Configuration.instance
  CapeZAssets.load_into(Capistrano::Configuration.instance)
end
