# Holds all Capistrano related tasks that can be shared across all sites
# Anything you put in this file must be generic enough to be used across all
# sites.
require 'capistrano'

class CapeZSolr
  def self.load_into(cap_config)
    cap_config.load do
      namespace :solr do
        desc :reindex, :roles => :app do
          run "cd #{current_release}/ezpublish_legacy && #{php_bin} extension/ezfind/bin/php/updatesearchindexsolr.php -s site_admin"
        end
      end
    end
  end
end


if Capistrano::Configuration.instance
  CapeZSolr.load_into(Capistrano::Configuration.instance)
end
