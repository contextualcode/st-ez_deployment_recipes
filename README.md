General purpose Capistrano deployment recipes
=============================================

This repository contains Capistrano recipes for deploying eZ Publish sites.

Usage
-----

To install these recipes onto an eZ Publish site you need to:

1. Create a Gemfile in the site root with the following contents:

        source 'https://rubygems.org'

        # Deployment recipes
        gem 'ez_deployment', :git => 'git@codebasehq.com:vision/capistrano/ez_recipes.git', :branch => 'master'

2. Run `bundle install` to install the gem.

3. Require the specific recipes you need in config/deploy.rb, e.g:

        begin
            require 'ezp_deployment/tools'
            require 'ezp_deployment/deploy_46'
        rescue LoadError
            puts "Could not load the required deployment receipies"
            puts "Make sure to run bundler to install the required dependencies"
        end
    
The begin/rescue clause is required if you have other tasks in
config/deploy.rb that invoke bundler or relies on any functionality provided by
`ezp_deployment`. If that is not the case you can remove it.

If you have made any changes to the recipes in this repository you can fetch
the latest changes by running `bundle update ez_deployment`. This will update
Gemfile.lock, so remember to commit it.
